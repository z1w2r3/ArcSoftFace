﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace BaiduFaceDllTest
{
    class BaiduFaceDll
    {
        [DllImport(@"BaiduFaceDll.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int Add(int v1, int v2);

        [DllImport(@"BaiduFaceDll.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int Init();

        [DllImport(@"BaiduFaceDll.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void UnInit();

        [DllImport(@"BaiduFaceDll.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int UserAdd(string user_id, string image, ref StringBuilder res);

        [DllImport(@"BaiduFaceDll.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int GetGroupList(ref StringBuilder res);

        [DllImport(@"BaiduFaceDll.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int UserDelete(string user_id, ref StringBuilder res);

        [DllImport(@"BaiduFaceDll.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int Identifier(string imgstr, ref StringBuilder res);

        [DllImport(@"BaiduFaceDll.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int RgbLivenessCheck(string imgstr, ref StringBuilder res);
    }
}

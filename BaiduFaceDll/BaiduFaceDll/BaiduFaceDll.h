#pragma once
#include "baidu_face_api.h"

#ifdef BAIDUFACEDLL_EXPORTS
#define BAIDUFACEDLL_API __declspec(dllexport)
#else
#define BAIDUFACEDLL_API __declspec(dllimport)
#endif

EXTERN_C BAIDUFACEDLL_API int Add(int a, int b);
EXTERN_C BAIDUFACEDLL_API int Init();
EXTERN_C BAIDUFACEDLL_API void UnInit();
EXTERN_C BAIDUFACEDLL_API int GetGroupList(char** res);
EXTERN_C BAIDUFACEDLL_API int UserAdd(const char* user_id, const char* image, char** res);
EXTERN_C BAIDUFACEDLL_API int UserDelete(const char* user_id, char** res);
EXTERN_C BAIDUFACEDLL_API int Identifier(const char* imgstr, char** res);
EXTERN_C BAIDUFACEDLL_API int RgbLivenessCheck(const char* imgstr, char** res);
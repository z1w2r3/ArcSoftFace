#include "stdafx.h"
#include "BaiduFaceDll.h"
using namespace std;

static BaiduFaceApi *api;
//测试程序
BAIDUFACEDLL_API int Add(int a, int b) {
	return a + b;
}
//初始化sdk
//返回值		dll返回值
BAIDUFACEDLL_API int Init()
{
	api = new BaiduFaceApi();
	return api->sdk_init();
}
//销毁sdk
BAIDUFACEDLL_API void UnInit() 
{
	delete api;
}
//添加用户
//user_id	用户id，字母、数字、下划线组成，最多128个字符
//image		图片的base64值
//res		返回dll值
//返回值		1表示正常0表示异常
BAIDUFACEDLL_API int GetGroupList(char** res)
{
	try {
		const char* rv = api->get_group_list(0,100);
		strcpy(*res, rv);
		return 1;
	}
	catch (exception ex) {
		return 0;
	}
}
//添加用户
//user_id	用户id，字母、数字、下划线组成，最多128个字符
//image		图片的base64值
//res		返回dll值
//返回值		1表示正常0表示异常
BAIDUFACEDLL_API int UserAdd(const char* user_id, const char* image,char** res)
{
	try {
		const char* rv = api->user_add(user_id, "group_1", image, 1);
		strcpy(*res, rv);
		return 1;
	}
	catch (exception ex) {
		return 0;
	}
}
//删除用户
//user_id	用户id，字母、数字、下划线组成，最多128个字符
//res		返回dll值
//返回值		1表示正常0表示异常
BAIDUFACEDLL_API int UserDelete(const char* user_id, char** res)
{
	try {
		const char* rv = api->user_delete(user_id, "group_1");
		strcpy(*res, rv);
		return 1;
	}
	catch (exception ex) {
		return 0;
	}
}
//人脸识别
//image		图片的base64值
//res		返回dll值
//返回值		1表示正常0表示异常
BAIDUFACEDLL_API int Identifier(const char* imgstr, char** res)
{
	try
	{
		const char* rv = api->identify(imgstr, 1, "group_1", "");
		strcpy(*res, rv);
		return 1;
	}
	catch (exception ex)
	{
		return 0;
	}
}
//rgb活体检测
//image		图片的base64值
//res		返回dll值
//返回值		1表示正常0表示异常
BAIDUFACEDLL_API int RgbLivenessCheck(const char* imgstr, char** res)
{
	try
	{
		const char* rv = api->rgb_liveness_check(imgstr, 1);
		strcpy(*res, rv);
		return 1;
	}
	catch (exception ex)
	{
		return 0;
	}
}

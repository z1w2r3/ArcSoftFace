﻿using System.Drawing;

namespace FaceIdentifier
{
    public interface FaceApi
    {
        void Init();

        void UnInit();

        string Match(Bitmap bitmap);

        void Add(string key, Bitmap bitmap);

        void Remove(string key);

        void ReloadFace();

        int FaceCount();

        PrivilegeInfo GetInfo(string key);
    }
}

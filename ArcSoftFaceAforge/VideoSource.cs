﻿using AForge.Video;
using AForge.Video.DirectShow;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArcSoftFaceAforge
{
    class VideoSource
    {
        public IVideoSource videoSource;

        public void Start()
        {
            //FilterInfoCollection filterInfoCollection = new FilterInfoCollection(FilterCategory.VideoInputDevice);
            //videoSource = new VideoCaptureDevice(filterInfoCollection[0].MonikerString);

            videoSource = new FileVideoSource("720p.avi");
            //videoSource.NewFrame += NewFrame;
            videoSource.Start();
        }

        public void Stop()
        {
            videoSource.Stop();
        }

    }
}

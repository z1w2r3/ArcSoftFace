﻿using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using AForge.Video;
using AForge.Video.DirectShow;
using Ini.Net;
using Newtonsoft.Json;
using AForge.Imaging.Filters;

namespace FaceIdentifier
{
    class VideoDeviceSource
    {
        private readonly object sync = new object();
        private IVideoSource _captureDevice;
        private IniFile config = new IniFile("相机配置.ini");
        private Bitmap currentFrame;
        public delegate void FPS(object sender, Bitmap bitmap);
        public event FPS _fps;
        private Pen pen = new Pen(new SolidBrush(Color.Yellow),5f);

        private bool requestStop = false;

        public void Start() {
            if(_captureDevice == null)
            {
                requestStop = false;
                //使用USB摄像机识别
                _captureDevice = GetCaptureDevice();
                //使用本地Resources目录下的720p.avi文件识别测试
                //_captureDevice = GetFileVideoSource();
                _captureDevice.NewFrame += _captureDevice_NewFrame; ;
                _captureDevice.Start();
            }
        }


        public void Stop()
        {
            if (_captureDevice == null) return;
            requestStop = true;
            _captureDevice.NewFrame -= _captureDevice_NewFrame;
            _captureDevice.Stop();
        }

        private void _captureDevice_NewFrame(object sender, NewFrameEventArgs eventArgs)
        {
            if (requestStop) return;
            Bitmap newFrame = (Bitmap)eventArgs.Frame.Clone();
            //如果使用视频文件请注释下面的这行代码，表示不对图像进行水平翻转
            newFrame.RotateFlip(RotateFlipType.Rotate180FlipY);
            lock (sync)
            {
                if (currentFrame != null)
                {
                    currentFrame.Dispose();
                    currentFrame = null;
                }
                currentFrame = newFrame;
                _fps.Invoke(sender, newFrame);
            }
        }

        public Bitmap GetCurrentFrame()
        {
            if (requestStop) return null;
            lock (sync)
            {
                return (currentFrame == null || currentFrame.Width == 0 || currentFrame.Height == 0) ? null : AForge.Imaging.Image.Clone(currentFrame);
            }
        }



        private VideoCaptureDevice GetCaptureDevice()
        {
            if (string.IsNullOrWhiteSpace(config.ReadString("相机配置", "MonikerString")))
            {
                ConfigCamera();
            }
            var MonikerString = config.ReadString("相机配置", "MonikerString");
            var FrameSize = config.ReadString("相机配置", "FrameSize");
            if (string.IsNullOrEmpty(MonikerString))
            {
                ConfigCamera();
                return GetCaptureDevice();
            }
            bool existDevice = false;
            FilterInfoCollection filters = new FilterInfoCollection(FilterCategory.VideoInputDevice);
            for (int i = 0; i< filters.Count; i++)
            {
                if (filters[0].MonikerString.Equals(MonikerString))
                {
                    existDevice = true;
                }
            }

            if (!existDevice)
            {
                ConfigCamera();
                return GetCaptureDevice();
            }

            var videoCaptureDevice = new VideoCaptureDevice(MonikerString);
            var capabilities = videoCaptureDevice.VideoCapabilities;
            foreach (var item in capabilities)
            {
                if (item.FrameSize.ToString().Equals(FrameSize))
                {
                    videoCaptureDevice.VideoResolution = item;
                    break;
                }
            }
            return videoCaptureDevice;
        }

        private FileVideoSource GetFileVideoSource()
        {
            return new FileVideoSource(Path.Combine("Resources","720p.avi"));
        }

        public void ConfigCamera()
        {
            var MonikerString = config.ReadString("相机配置", "MonikerString");
            var FrameSize = config.ReadString("相机配置", "FrameSize");

            VideoCaptureDeviceForm deviceForm = new VideoCaptureDeviceForm();
            deviceForm.TopMost = true;
            if (!string.IsNullOrEmpty(FrameSize) && _captureDevice != null && _captureDevice.GetType() == typeof(VideoCaptureDevice))
            {
                VideoCaptureDevice vcd = (VideoCaptureDevice)_captureDevice;
                VideoCapabilities[] vcs = vcd.VideoCapabilities;
                foreach(var item in vcs)
                {
                    if (item.FrameSize.ToString().Equals(FrameSize))
                    {
                        deviceForm.CaptureSize = item.FrameSize;
                        break;
                    }
                }
            }
            
            DialogResult dr = deviceForm.ShowDialog();
            //如果未加载到相机,则系统退出
            if(deviceForm.VideoDevice == null)
            {
                MessageBox.Show("未加载到相机,请退出软件后检查相机是否正常");
                Environment.Exit(0);
            }

            //如果从未设置过，第一次又点了取消，则默认使用第一选项
            if((string.IsNullOrEmpty(MonikerString) || string.IsNullOrEmpty(FrameSize)) && DialogResult.Cancel == dr)
            {
                MessageBox.Show("未配置相机,软件将退出");
                Environment.Exit(0);
            }
            //如果确认当前选项，则使用当前选中配置
            if(DialogResult.OK == dr)
            {
                config.WriteString("相机配置", "MonikerString", deviceForm.VideoDeviceMoniker);
                config.WriteString("相机配置", "FrameSize", deviceForm.VideoDevice.VideoResolution.FrameSize.ToString());
            }
        }

        public void PaintToPictureBox(PictureBox pictureBox1, Graphics g)
        {
            if (requestStop) return;
            using(Bitmap bitmap = GetCurrentFrame())
            {
                if (currentFrame == null)
                {
                    return;
                }

                if (pictureBox1.Image == null)
                {
                    pictureBox1.Image = new Bitmap(pictureBox1.Width, pictureBox1.Height);
                    return;
                }
                int w = pictureBox1.Width;
                int h = pictureBox1.Height;
                int x = (w - bitmap.Width) / 2;
                int y = (h - bitmap.Height) / 2;
                g.DrawImage(bitmap, x, y);

                int point = 30;
                int point_width = 80;
                g.DrawLines(pen, new PointF[] { new PointF(point_width, point) ,new PointF(point, point), new PointF(point, point_width) });
                g.DrawLines(pen, new PointF[] { new PointF(w - point_width, point), new PointF(w -point, point), new PointF(w - point, point_width) });
                g.DrawLines(pen, new PointF[] { new PointF(point,h-point_width), new PointF(point, h-point), new PointF(point_width, h-point) });
                g.DrawLines(pen, new PointF[] { new PointF(w-point_width,h-point), new PointF(w-point, h-point), new PointF(w - point, h - point_width) });

            }
        }

        internal Bitmap GetCurrentCropFrame(Size pictureBoxSize)
        {
            using(var bitmap = GetCurrentFrame())
            {
                if (bitmap == null) return null;
                if (pictureBoxSize == null || pictureBoxSize.IsEmpty) return bitmap;

                int x = Math.Max(0, (bitmap.Width - pictureBoxSize.Width) / 2);
                int y = Math.Max(0, (bitmap.Height - pictureBoxSize.Height) / 2);
                //只识别显示在pictureBox区域的人脸
                var crop = new Crop(new Rectangle(new Point(x, y), pictureBoxSize));
                var cropBitmap = crop.Apply(bitmap);
                return cropBitmap;
            }
        }
    }
}

﻿using System.IO;
using Ini.Net;

namespace FaceIdentifier
{
    internal class ServiceConfig
    {
        private const string fileName = "服务器服务配置.ini";
        private readonly IniFile iniFile = new IniFile(fileName);

        public ServiceConfig()
        {
            if (!HasConfig())
            {
                SetUpdateUrl("http://119.23.45.198/FaceIdentifier/updateVersion.xml");
                SetUrl("http://127.0.0.1:9091");
                SetOpenDoorCode("01 57 00 01 00 01 D0 02 01 03 A6");
            }
        }

        public bool HasConfig()
        {
            return File.Exists(fileName);
        }

        public string Url()
        {
            return iniFile.ReadString("人脸服务器地址", "url");
        }

        public void SetUrl(string url)
        {
            iniFile.WriteString("人脸服务器地址", "url", url);
        }

        public string UpdateUrl()
        {
            return iniFile.ReadString("软件更新地址", "updateUrl");
        }

        public void SetUpdateUrl(string updateUrl)
        {
            iniFile.WriteString("软件更新地址", "updateUrl", updateUrl);
        }

        public void SetOpenDoorCode(string openDoorCode)
        {
            iniFile.WriteString("开闸协议", "openDoorCode", openDoorCode);
        }

        public string getOpenDoorCode()
        {
            return iniFile.ReadString("开闸协议", "openDoorCode");
        }
    }
}
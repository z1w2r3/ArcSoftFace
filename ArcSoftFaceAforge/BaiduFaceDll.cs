﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;

namespace FaceIdentifier
{
    class BaiduFaceDll
    {
        [DllImport(@"BaiduFaceDll.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int Add(int v1, int v2);

        [DllImport(@"BaiduFaceDll.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int Init();

        [DllImport(@"BaiduFaceDll.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void UnInit();

        [DllImport(@"BaiduFaceDll.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int UserAdd(string user_id, string image, ref StringBuilder res);

        [DllImport(@"BaiduFaceDll.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int GetGroupList(ref StringBuilder res);

        [DllImport(@"BaiduFaceDll.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int UserDelete(string user_id, ref StringBuilder res);

        [DllImport(@"BaiduFaceDll.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int Identifier(string imgstr, ref StringBuilder res);

        [DllImport(@"BaiduFaceDll.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int RgbLivenessCheck(string imgstr, ref StringBuilder res);

        internal static string base64FromBitmap(Bitmap bitmap)
        {
            try
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    bitmap.Save(ms, ImageFormat.Jpeg);
                    byte[] bytes = new byte[ms.Length];
                    ms.Position = 0;
                    ms.Read(bytes, 0, bytes.Length);
                    return Convert.ToBase64String(bytes);
                }
            }
            catch (Exception)
            {

                return null;
            }
        }
    }
}

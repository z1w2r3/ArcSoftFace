﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;

namespace FaceIdentifier
{
    public class BaiduFaceApiImp : FaceApi
    {
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(typeof(BaiduFaceApiImp));
        private readonly Dictionary<string, PrivilegeInfo> _file = new Dictionary<string, PrivilegeInfo>();
        private StringBuilder buffer = new StringBuilder(1000);
        private string Repository = "人脸库";

        public void UnInit()
        {
            BaiduFaceDll.UnInit();
        }

        void FaceApi.Add(string key, Bitmap bitmap)
        {
            AddBitmap(key, bitmap);
        }

        int FaceApi.FaceCount()
        {
            return _file.Count;
        }

        PrivilegeInfo FaceApi.GetInfo(string key)
        {
            if (_file.Count == 0)
            {
                var files = Directory.GetFiles(Repository).Where(w => w.EndsWith("jpg"));
                foreach (var file in files)
                {
                    try
                    {
                        FileInfo fi = new FileInfo(file);
                        var fileNames = fi.Name.Replace(fi.Extension, "").Split('_');

                        PrivilegeInfo info = new PrivilegeInfo();
                        info.personSN = fileNames[0];
                        info.personName = fileNames[1];

                        _file.Add(info.personSN, info);
                    }
                    catch (Exception e)
                    {
                        Log.Error("添加人脸到对比库异常", e);
                    }
                }
            }
            PrivilegeInfo pi;
            _file.TryGetValue(key, out pi);
            return pi;
        }

        void FaceApi.Init()
        {
            BaiduFaceDll.Init();
        }

        string FaceApi.Match(Bitmap bitmap)
        {
            Log.Debug("正在识别");
            
            var base64 = BaiduFaceDll.base64FromBitmap(bitmap);
            if (string.IsNullOrEmpty(base64))
            {
                return null;
            }

            buffer.Clear();
            BaiduFaceDll.Identifier(base64, ref buffer);
            string result = buffer.ToString();
            Log.InfoFormat("result {0}", result);
            if (string.IsNullOrEmpty(result))
            {
                return null;
            }
            JObject jobject = JObject.Parse(result);
            int errno = Convert.ToInt32(jobject["errno"]);
            //未检测到人
            if (errno != 0)
            {
                return null;
            }
            JArray results = (JArray)jobject["data"]["result"];
            foreach (JObject j in results)
            {
                if (Convert.ToDouble(j["score"]) > 70)
                {
                    return Convert.ToString(j["user_id"]);
                }
            }
            return "";
        }

        void FaceApi.ReloadFace()
        {
            Log.InfoFormat("准备加载本地人脸库:{0}", Repository);
            if (!Directory.Exists(Repository))
            {
                Log.InfoFormat("创建本地对比库:{0}", Repository);
                Directory.CreateDirectory(Repository);
                return;
            }

            var files = Directory.GetFiles(Repository).Where(w => w.EndsWith("jpg"));
            foreach (var file in files)
            {
                try
                {
                    var fi = new FileInfo(file);
                    var name = fi.Name.Replace(fi.Extension, "");

                    using (var bitmap = new Bitmap(file))
                    {
                        var key = name.Split('_')[0];
                        AddBitmap(key, bitmap);
                    }

                }
                catch (Exception e)
                {
                    Log.Error("添加人脸到对比库异常", e);
                }
            }
        }

        private void AddBitmap(string key, Bitmap bitmap)
        {
            buffer.Clear();
            int i = BaiduFaceDll.UserAdd(key, BaiduFaceDll.base64FromBitmap(bitmap), ref buffer);
            Log.DebugFormat("user_add {0}", i);
        }

        void FaceApi.Remove(string key)
        {
            buffer.Clear();
            BaiduFaceDll.UserDelete(key, ref buffer);
        }
    }
}

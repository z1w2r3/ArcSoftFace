# ArcSoftFace

#### 项目介绍
- 基于人脸识别SDK开发的通道闸客户端，分辨率为800x600的小型电脑，目前己投入使用。

#### 软件架构
- vs2015 + 虹软SDK/百度SDK + .net4.5.2 + windows 64

#### 安装教程

- 安装.net4.5.2 https://download.microsoft.com/download/E/2/1/E21644B5-2DF2-47C2-91BD-63C560427900/NDP452-KB2901907-x86-x64-AllOS-ENU.exe
- 安装 vcredist_x64 https://www.microsoft.com/zh-cn/download/confirmation.aspx?id=30679&6B49FDFB-8E5B-4B07-BC31-15695C5A2143=1
- 设置vs2015 为 x64位开发平台

#### 使用说明

- 启动Main后，可以在视频区域单击右键进行【拍照】，输入编号与姓名，单击保存即可将当前画面中出现的头像保存到人脸对比库中。人脸库目录： “运行目录/人脸库”
- 可以在视频区域单击右键选择【显示方框】，这样可以实时显示头像在视频中的位置与l人脸当前的宽度，宽度小于80，系统将不识别，因为离闸机太远，识别后开闸还来不及通过闸又会关闭。
- 所有人脸识别记录将保存在 “运行目录/识别记录” 目录下。
- 系统默认使用USB摄像头做识别，可以使用提供的视频文件做测试看效果。在resource目录下存在一个720p.avi的小品文件。直接修改源码中的这个位置即可使用这个视频文件做测试

VideoDeviceSource.cs
```
public void Start() {
    if(_captureDevice == null)
    {
        requestStop = false;
        //使用USB摄像机识别
        _captureDevice = GetCaptureDevice();
        //使用本地Resources目录下的720p.avi文件识别测试
        //_captureDevice = GetFileVideoSource();
        _captureDevice.NewFrame += _captureDevice_NewFrame; ;
        _captureDevice.Start();
    }
}

private void _captureDevice_NewFrame(object sender, NewFrameEventArgs eventArgs)
{
    if (requestStop) return;
    Bitmap newFrame = (Bitmap)eventArgs.Frame.Clone();
    //如果使用视频文件请注释下面的这行代码，表示不对图像进行水平翻转
    newFrame.RotateFlip(RotateFlipType.Rotate180FlipY);
    lock (sync)
    {
        if (currentFrame != null)
        {
            currentFrame.Dispose();
            currentFrame = null;
        }
        currentFrame = newFrame;
        _fps.Invoke(sender, newFrame);
    }
}
                   
```


#### 截图

![输入图片说明](https://images.gitee.com/uploads/images/2018/0720/135832_a934e644_87848.png "221746vgo370tvt7ii7inv.png")

![输入图片说明](https://images.gitee.com/uploads/images/2018/0720/135843_4fe069f3_87848.png "221759y9oi09kla9dm3aod.png")

![输入图片说明](https://images.gitee.com/uploads/images/2018/0720/135853_c7081d7e_87848.png "221957mx6zeerqwwlt9xlr.png")
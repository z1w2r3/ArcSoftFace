﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace FaceIdentifier
{
    public class BaiduFaceDll
    {
        [DllImport(@"BaiduFaceDll.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int Add(int v1, int v2);

        [DllImport(@"BaiduFaceDll.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int Init();

        [DllImport(@"BaiduFaceDll.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void UnInit();

        [DllImport(@"BaiduFaceDll.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int UserAdd(string user_id, string image, ref StringBuilder res);

        [DllImport(@"BaiduFaceDll.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int GetGroupList(ref StringBuilder res);

        [DllImport(@"BaiduFaceDll.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int UserDelete(string user_id, ref StringBuilder res);

        [DllImport(@"BaiduFaceDll.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int Identifier(string imgstr, ref StringBuilder res);

        [DllImport(@"BaiduFaceDll.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern int RgbLivenessCheck(string imgstr, ref StringBuilder res);

        public static string base64FromBitmap(Bitmap bmp1)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                bmp1.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                byte[] arr1 = new byte[ms.Length];
                ms.Position = 0;
                ms.Read(arr1, 0, (int)ms.Length);
                ms.Close();
                return Convert.ToBase64String(arr1);
            }
        }
    }
}
